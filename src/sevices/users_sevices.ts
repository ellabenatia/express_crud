import { users_type } from "../interfaces/users_list.js";
import * as db from "../db/data.js";

const uid = () => Math.random().toString(36).substring(6);

export function getUserByid(users:users_type[], id:string) {
    for(let user of users){
        if(user.id===id){
            return user;
        }
    }
return undefined;
}

export function createUser(users:users_type[],firstname:string,lastname:string,email:string):users_type{
        let new_user: users_type = {
        id: uid(),
        first_name:firstname as string,
        last_name: lastname as string,
        email: email,   
        }
      users.push(new_user);
      db.save(users);
      return new_user;
}

export function deleteById(users:users_type[],id_user:string):users_type[]{
    users = users.filter((user:users_type) => user.id!==id_user);
    db.save(users);
    return users;
}



