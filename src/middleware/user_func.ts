import { NextFunction, Request, Response } from 'express';
import fs from 'fs/promises';

export async function user_log(req:Request,res:Response,next:NextFunction){
    let time: number = Date.now();
    let type = req.method;
    let path = req.url;
    let new_log:string = `${type} ${path} / ${time} \n`;
    await fs.appendFile('./src/log/users.log',new_log);
    console.log();
    next();
}