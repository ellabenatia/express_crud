import fs from 'fs';
import { NextFunction, Request, Response } from 'express';
import log from '@ajar/marker';
import url_not_found from '../exceptions/urlNotFound.js';
import HttpException from '../exceptions/http_eception.js';

export function urlNotFound (req:Request,res:Response,next:NextFunction){
    next(new url_not_found(`404 eror ${req.url} not found`));
 }
 export function logError(){
     log.yellow("got here");
     const errorsFileLogger = fs.createWriteStream('./src/log/error.log', { flags: 'a' });
     return (err:HttpException, req: Request, res: Response, next: NextFunction) => {
         errorsFileLogger.write(`${req.id} : ${err.message} >> ${err.stack} \n`);
         next(err);
     }
 }

 export function res_error(err:HttpException,req:Request,res:Response,next:NextFunction){
     console.log("error endler", err.message);
     res.status(500).json({"status" : err.message});
 }