import { Request, Response, NextFunction, Router } from "express";
import { users_type } from "../interfaces/users_list.js";
import * as db from "../db/data.js";
import { create_user, delete_user, get_user, update_user } from "../controller/users_func.js";


const router = Router();

router.use(async (req: Request, res: Response, next: NextFunction) => {
    const users: users_type[] = await db.getData();
    req.body.users = users;
    next();
  });

router.get('/read/:id',get_user);
router.post("/create",create_user);
router.delete('/delete/:id',delete_user);
router.put("/update/",update_user);

export default router;
