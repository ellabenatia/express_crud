import {Request,Response,NextFunction} from 'express';
import server_error from '../exceptions/server_eception.js';
import { users_type } from '../interfaces/users_list.js';
import * as db from "../db/data.js";
import log from "@ajar/marker";
import { createUser, deleteById, getUserByid } from '../sevices/users_sevices.js';

const uid = () => Math.random().toString(36).substring(6);

export function get_user(req:Request,res:Response,next:NextFunction){
  try{
       let {users} = req.body;
    let user = getUserByid(users,req.params.id);
    if(user === undefined){
      throw new server_error("users is undefined");
    }
    else{
        res.status(200).json(user);
        }
    res.send();
  }
  catch(err){
    next((err as server_error));
  }
  }

  export async function create_user(req:Request, res:Response, next:NextFunction){
    try{
      if(req.body.firstname === undefined){
        throw new server_error("firstname is undefined");
      }
      if(req.body.lastname === undefined){
        throw new server_error("lastname is undefined");
      }
      if(req.body.email === undefined){
        throw new server_error("email is undefined");
      }
      let user = createUser(req.body.users,req.body.firstname,req.body.lastname,req.body.email);
      res.status(200).json(user);
     }
    catch(error){
        next((error as server_error));
  }
  }
export function delete_user(req:Request, res:Response){
  let users = deleteById(req.body.users,req.params.id);
  res.status(200).json(users);
  }

export async function update_user(req:Request, res:Response){
 deleteById(req.body.users,req.params.id);
  let user = createUser(req.body.users,req.body.firstname,req.body.lastname,req.body.email);
  res.status(200).json(user);
  }