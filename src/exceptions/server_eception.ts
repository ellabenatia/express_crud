
import HttpException from "./http_eception.js";
class server_error extends HttpException{
    status: number;
    constructor(message: string){
    super(message);
    this.status = 400;
    }

}

export default server_error;
