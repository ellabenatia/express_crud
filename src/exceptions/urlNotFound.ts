
import HttpException from "./http_eception.js";
class url_not_found extends HttpException{
    status: number;
    constructor(message: string){
    super(message);
    this.status = 404;
    }
}
export default url_not_found;
 