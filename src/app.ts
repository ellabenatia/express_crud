import express,{Request,Response,NextFunction} from 'express';
import morgan from 'morgan';
import {urlNotFound,logError,res_error} from './middleware/err_func.js'
import {user_log} from './middleware/user_func.js'

import log from '@ajar/marker';
import apiRouter from './routers/user.js';

const uid = () => Math.random().toString(36).substring(6);

const { PORT=8080, HOST='localhost'} = process.env;

const app = express()

const genId  = (req:Request,res:Response,next:NextFunction)=>{
    req.id = uid();
    next();
}
app.use(user_log);
app.use(morgan('dev'));
app.use(express.json());

app.use(genId);
app.use('/user',apiRouter);

//err_endlers
app.use(urlNotFound);
app.use(logError());
app.use(res_error)


app.listen(Number(PORT), HOST, ()=> {
    log.magenta(`🌎  listening on`,`http://${HOST}:${PORT}`);
});