import fs from 'fs/promises';
import {users_type} from '../interfaces/users_list.js';


export async function getData(): Promise<users_type[]> {
    const fileExists = async (path: string) =>
      !!(await fs.stat(path).catch((e) => false));
  
    if (await fileExists('./users.json')) {
      return JSON.parse(await fs.readFile('./users.json', "utf-8"));
    } else {
      return [] as users_type[];
    }
  }
  
export async function save(users:users_type[]){
    await fs.writeFile('./users.json', JSON.stringify(users, null, 2));
}